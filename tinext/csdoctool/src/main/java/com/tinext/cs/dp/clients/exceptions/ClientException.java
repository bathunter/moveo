package com.tinext.cs.dp.clients.exceptions;

public class ClientException extends Exception{
    public ClientException(String message) {
        super(message);
    }
    public ClientException(String message, Throwable cause) {
        super(message, cause);
    }
}
