package com.tinext.cs.dp;

import com.tinext.cs.dp.clients.Client;
import com.tinext.cs.dp.clients.Csclient;
import com.tinext.cs.dp.clients.exceptions.ClientException;
import com.tinext.cs.dp.model.Cstask;
import com.tinext.cs.dp.model.Ixattr;
import ixos.dt2.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

import static com.tinext.cs.dp.Constants.*;
import static ixos.dp2.Constants.OP_ERROR;
import static ixos.dp2.Constants.OP_OK;

public class Csdoctool extends Doctool {
    private static final Log LC = LogFactory.getLog(Csdoctool.class);
    private final static List<String> KEYS = Arrays.asList(TINEXT_FIELDS.toString(), TINEXT_LRID.toString());

    private Csdoctool() {
        super();
    }

    public static void main(String[] args) {
        LC.debug("Create instance of this docTool.");
        Doctool dt = new Csdoctool();
        LC.debug("Call the run() method.");
        dt.run(args);
        LC.debug("Return from main() method.");
    }

    public void doBeforeConnect() {
        if (LC.isTraceEnabled()) {
            LC.trace("enter");
        }
        LC.info("Csdoctool " + Version.version + " (Build " + Version.revision + ")");
        super.doBeforeConnect();
        if (LC.isTraceEnabled()) {
            LC.trace("leave");
        }
    }

    protected Config createNewConfig() {
        return new Csconfig(this);
    }

    @Override
    public void service(Task task) throws DoctoolException {
        if (LC.isTraceEnabled()) {
            LC.trace("enter");
        }
        String MN = "service ";
        LC.info(MN + "new document: " + task.getDocDir().getPath());
        Cstask cstask = new Cstask(this, task.getDocDir());
        try {
            Commands commands = cstask.getCommands();
            Ixattr ixattr = cstask.getIxattr();
            if (hasAllKeys(commands.getEntries())) {
                List<String> dirtyList =
                        new ArrayList<>(Arrays.asList(commands.getValue(TINEXT_FIELDS.toString()).split("\\s" + "*," + "\\s*")));
                List<String> params = new ArrayList<>();
                dirtyList.forEach(e -> {
                    if (e.startsWith("cmd_")) {
                        String cmd = e.substring("cmd_".length());
                        String cmdValue = commands.getValue(cmd);
                        if (cmdValue != null) {
                            if (cmd.equals(LLNODEINFO.toString())) {
                                params.add(cmdValue.replaceAll("-[0-9]+\\s", ""));
                            } else {
                                params.add(fromBase64(cmdValue));
                            }
                        }
                    } else if (e.startsWith("xattr_")) {
                        String xattrValue = ixattr.getValue(e.substring("xattr_".length()));
                        if (xattrValue != null) {
                            params.add(fromBase64(xattrValue));
                        }
                    }
                });
                if (!params.isEmpty()) {
                    long reportID = Long.parseLong(commands.getValue(TINEXT_LRID.toString()));
                    Client client = new Csclient();
                    if (client.isTimedOut()) {
                        client.refresh();
                    }
                    client.runReport(reportID, params);
                } else {
                    cstask.writeDPprotocol(NOPARAMS.toString());
                    LC.debug(MN + NOPARAMS.toString());
                }
            } else {
                cstask.writeDPprotocol(String.format(NOTFOUND.toString(), KEYS.toString()));
                LC.debug(MN + String.format(NOTFOUND.toString(), KEYS.toString()));
            }
            cstask.setComment(DONE.toString());
        } catch (ClientException | NumberFormatException e) {
            cstask.writeDPprotocol(OP_ERROR + " : " + e.getMessage());
            LC.error(MN + e.getMessage(), e);
            throw new DoctoolException(OP_ERROR, e.getMessage());
        }
        cstask.writeDPprotocol(OP_OK);
        if (LC.isTraceEnabled()) {
            LC.trace("leave");
        }
    }
    //    private boolean hasAllKeys(HashMap<String, String> entries) {
    //        return entries.keySet().containsAll(KEYS);
    //    }

    private boolean hasAllKeys(List<Commands.Entry> entries) {
        return entries.stream().map(Commands.Entry::getKey).collect(Collectors.toList()).containsAll(KEYS);
    }

    private String fromBase64(String encoded) {
        if (encoded.matches("[0-9]+")) {
            return encoded;
        }
        try {
            return new String(Base64.getDecoder().decode(encoded));
        } catch (IllegalArgumentException e) {
            return encoded;
        }
    }
}
