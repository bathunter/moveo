package com.tinext.cs.dp.cws.exceptions;

/**
 * Created by skorotkov on 7/2/2017.
 */
public class DocumentManagementException extends Exception {
    public DocumentManagementException(String message) {
        super(message);
    }
    public DocumentManagementException(String message, Throwable cause) {
        super(message, cause);
    }
}
