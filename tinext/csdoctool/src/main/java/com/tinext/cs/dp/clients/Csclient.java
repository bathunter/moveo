package com.tinext.cs.dp.clients;

import com.tinext.cs.dp.clients.exceptions.ClientException;
import com.tinext.cs.dp.setup.SetupReader;
import com.tinext.cs.dp.cws.Authenticator;
import com.tinext.cs.dp.cws.DocumentManager;
import com.tinext.cs.dp.cws.exceptions.AuthenticationException;
import com.tinext.cs.dp.cws.exceptions.DocumentManagementException;
import ixos.base.PwdCrypt;
import ixos.base.setup.SetupException;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

import static com.tinext.cs.dp.Constants.HTTP_PASSWORD;
import static com.tinext.cs.dp.Constants.HTTP_USERNAME;

public class Csclient implements Client {
//    private final static String USERNAME = PwdCrypt.pwdecrypt(SetupReader.getVar(HTTP_USERNAME.toString(),"Admin"));
//    private final static String PASSWORD = PwdCrypt.pwdecrypt(SetupReader.getVar(HTTP_PASSWORD.toString(),"livelink"));
    private final static Integer SHIFT = 5;

    private DocumentManager documentManager;
    private Date expirationDate;


    private void init() throws ClientException {
        Authenticator authenticator;

        try {
            String USERNAME = PwdCrypt.pwdecrypt(SetupReader.getVar(HTTP_USERNAME.toString(),"Admin"));
            String PASSWORD = PwdCrypt.pwdecrypt(SetupReader.getVar(HTTP_PASSWORD.toString(),"livelink"));
            authenticator = new Authenticator(USERNAME, PASSWORD);
            authenticator.init();
            if (authenticator.hasToken()) {
                expirationDate = authenticator.getExpDate();
                String authToken = authenticator.getAuthToken();
                documentManager = new DocumentManager();
                documentManager.init(authToken);
            }
        } catch (AuthenticationException | DocumentManagementException | SetupException e) {
            throw new ClientException(e.getMessage(), e);
        }
    }


    @Override
    public boolean isTimedOut() {
        return (null == expirationDate) || expirationDate.compareTo(dateWithShift()) < 0;
    }

    @Override
    public void refresh() throws ClientException {
        init();
    }

    @Override
    public <T> void runReport(long reportID, List<T> inputs) throws ClientException {
        try {
            documentManager.runReport(reportID, inputs);
        } catch (DocumentManagementException e) {
            throw new ClientException(e.getMessage());
        }
    }


    private Date dateWithShift() {
        LocalDateTime dateTime = LocalDateTime.now().plus(Duration.of(SHIFT, ChronoUnit.MINUTES));
        return Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
}
