package com.tinext.cs.dp.model;

import ixos.dt2.Doctool;
import ixos.dt2.DoctoolException;
import ixos.dt2.Task;

import java.io.File;
import java.io.IOException;


public class Cstask extends Task {

    private Ixattr ixattr;

    public Cstask(Doctool doctool, File file) {
        super(doctool, file);
    }

    public final synchronized Ixattr getIxattr() throws DoctoolException {
        if (ixattr == null) {
            try {
                ixattr = new Ixattr(this.getDocDir().getAbsolutePath());
                ixattr.read();
            } catch (IOException e) {
                throw new DoctoolException("Cannot open IXATTR file", e);
            }
        }
        return ixattr;
    }
}
