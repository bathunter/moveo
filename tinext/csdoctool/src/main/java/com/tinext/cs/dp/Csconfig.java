package com.tinext.cs.dp;

import ixos.dt2.Config;
import ixos.dt2.Doctool;
import java.io.PrintStream;

public class Csconfig extends Config {

    Csconfig(Doctool doctool) {
        super(doctool);
    }


    protected void printArgsUsage(PrintStream ps) {
        super.printArgsUsage(ps);
    }


    protected void printHintsUsage(PrintStream ps) {
        super.printHintsUsage(ps);
        ps.println("\tThis program is a custom doctool");
    }
}
