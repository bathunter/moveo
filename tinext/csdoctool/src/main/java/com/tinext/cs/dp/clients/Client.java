package com.tinext.cs.dp.clients;

import com.tinext.cs.dp.clients.exceptions.ClientException;

import java.util.List;

public interface Client {
    boolean isTimedOut();

    void refresh() throws ClientException;

    <T> void runReport(long reportID, List<T> inputs) throws ClientException;
}
