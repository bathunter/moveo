package com.tinext.cs.dp.model;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Ixattr {
    private final static String SUBTITLE = "ATTRIBUTES ";
    private final static String IXATTR_FILE = "IXATTR";
    private static final Log LC = LogFactory.getLog(Ixattr.class);
    private String filename;
    private List<Ixattr.Attribute> attributeList;

    Ixattr(String filename) {
        if (LC.isTraceEnabled()) {
            LC.trace("Entry: currentDir=" + filename);
        }
        this.filename = filename + File.separator + IXATTR_FILE;
        if (LC.isTraceEnabled()) {
            LC.trace("Exit");
        }
    }


    private Ixattr.Attribute getAttribute(String key) {
        return attributeList.stream().filter(a -> a.getKey().equalsIgnoreCase(key)).findAny().orElse(null);
    }

    public String getValue(String key) {
        return (null != getAttribute(key)) ? getAttribute(key).getValue() : null;
    }

/*    public String getType(String key) {
        return (null != getAttribute(key)) ? getAttribute(key).getType() : null;
    }*/


    void read() throws IOException {
        attributeList = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(filename), StandardCharsets.ISO_8859_1)) {
            stream.forEach(l -> {
                Ixattr.Attribute att = parse(l);
                if (att != null) {
                    attributeList.add(parse(l));
                }
            });
        }
    }

    private Ixattr.Attribute parse(String str) {
        if (str == null || str.isEmpty()) {
            LC.error(String.format("IXATTR parse error: string %s is empty", str));
            return null;
        }
        List<String> parts = new ArrayList<>(Arrays.asList(str.split("" + "\\|" + "*\\|")));
        if (parts.isEmpty() || parts.size() != 4 || !parts.get(0).equals(SUBTITLE)) {
            LC.error(String.format("IXATTR parse error: string %s is not valid", str));
            return null;
        }
        return new Ixattr.Attribute(parts.get(1), parts.get(2), parts.get(3));
    }

    public final class Attribute implements Serializable {
        private static final long serialVersionUID = -4375602179866529451L;
        private String key;
        private String type;
        private String value;

        Attribute(String key, String type, String value) {
            if (key == null) {
                throw new NullPointerException("key cannot be null");
            }
            if (type == null) {
                throw new NullPointerException("type cannot be null");
            } else {
                this.key = key;
                this.type = type;
                this.value = value;
            }
        }

        String getKey() {
            return key;
        }

        String getValue() {
            return value;
        }

        String getType() {
            return type;
        }

        public int hashCode() {
            return (this.key == null ? 0 : this.key.hashCode()) ^ (this.value == null ? 0 : this.value.hashCode());
        }

        public boolean equals(Object toCompare) {
            if (toCompare != null && toCompare.getClass() == this.getClass()) {
                if (toCompare == this) {
                    return true;
                } else {
                    Attribute attToCompare = (Attribute) toCompare;
                    String key = this.getKey();
                    String keyToCompare = attToCompare.getKey();
                    if (key != null && key.equals(keyToCompare)) {
                        String type = this.getType();
                        String typeToCompare = attToCompare.getType();
                        if (type != null && type.equals(typeToCompare)) {
                            String value = this.getValue();
                            String valueToCompate = attToCompare.getValue();
                            return value != null && value.equals(valueToCompate);
                        }
                    }
                    return false;
                }
            } else {
                return false;
            }
        }

        public String toString() {
            return this.getValue() == null ? this.getKey() : this.getKey() + " " + this.getValue();
        }
    }
}
