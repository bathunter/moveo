package com.tinext.cs.dp.setup;

import ixos.base.log4j.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class SetupPropsReader {
    private final static String CONF_DIR = "ECM_DOCUMENT_PIPELINE_CONF";
    private final static String SETUP_FILE = "DT_DOCTOLES.Setup";
    private final static SetupPropsReader INSTANCE = new SetupPropsReader();
    private final static Log.Context LC = new Log.Context("com.tinext.cs.dp.setup.SetupPropsReader");
    private final java.util.Properties prop = new java.util.Properties();


    private SetupPropsReader() {
        String conf = System.getenv(CONF_DIR);
        String filename = (conf != null) ?
                conf + File.separator + "setup" + File.separator + "setup" + File.separator + SETUP_FILE : SETUP_FILE;
        try (InputStream inputStream = new FileInputStream(filename)) {
            prop.load(inputStream);
        } catch (IOException e) {
            LC.error("", String.format("Error reading %s file ", filename), e.getCause());
            System.exit(-1);
        }
    }


    public static String getProperty(String key) {
        String value = INSTANCE.prop.getProperty(key);
        return (null == value) ? "" : value.trim();
    }
}
