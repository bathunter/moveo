package com.tinext.cs.dp.setup;

import ixos.base.setup.SetupException;
import ixos.base.setup.SetupFile;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;

import static com.tinext.cs.dp.Constants.DT_SETUP;
import static com.tinext.cs.dp.Constants.ECM_DOCUMENT_PIPELINE_CONF;

public class SetupReader {
    private static final Log LC = LogFactory.getLog(SetupReader.class);
    private static SetupReader setupReader;
    private SetupFile setupFile;

    private SetupReader() throws SetupException {
        String conf = System.getenv(ECM_DOCUMENT_PIPELINE_CONF.toString());
        String filename = (conf != null) ?
                conf + File.separator + "config" + File.separator + "setup" + File.separator + DT_SETUP.toString() :
                DT_SETUP.toString();
        //        try {
        setupFile = new SetupFile("", filename);
        setupFile.readConfig();
        //        } catch (SetupException e) {
        //            LC.error(String.format("Error reading %s file: %s", filename, e.getCause()), e);
        //            throw new SetupException("Error reading %s file: %s", filename, e.getCause());
        //            //System.exit(-1);
        //        }
    }

    private static SetupReader getReader() throws SetupException {
        if (setupReader == null) {
            setupReader = new SetupReader();
        }
        return setupReader;
    }

    public static String getVar(String key, String defaultValue) throws SetupException {
        String value = getReader().setupFile.getVar(key, defaultValue);
        return (null == value) ? "" : value.trim();
    }
}
