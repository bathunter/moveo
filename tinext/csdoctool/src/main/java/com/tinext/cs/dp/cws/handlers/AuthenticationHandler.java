package com.tinext.cs.dp.cws.handlers;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.util.Set;
import java.util.TreeSet;

import static com.tinext.cs.dp.Constants.*;

public class AuthenticationHandler implements SOAPHandler<SOAPMessageContext> {

    private String authToken;

    public AuthenticationHandler(String authToken) {
        this.authToken = authToken;
    }

    public Set<QName> getHeaders() {
        return new TreeSet();
    }

    public boolean handleMessage(SOAPMessageContext context) {
        Boolean outboundProperty = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        if (outboundProperty) {
            try {
                SOAPEnvelope envelope = context.getMessage().getSOAPPart().getEnvelope();
                SOAPFactory factory = SOAPFactory.newInstance();
                SOAPHeader header = envelope.getHeader();
                SOAPElement otAuthElement = factory.createElement(OTAUTHENTICATION.toString(), PREFIX.toString(),
                        ECM_API_NAMESPACE.toString());
                SOAPElement authTokenElement = factory.createElement(AUTHENTICATIONTOKEN.toString(),
                        PREFIX.toString(), ECM_API_NAMESPACE.toString());
                authTokenElement.addTextNode(authToken);
                otAuthElement.addChildElement(authTokenElement);
                header.addChildElement(otAuthElement);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    public boolean handleFault(SOAPMessageContext context) {
        return false;
    }

    public void close(MessageContext context) {
    }
}
