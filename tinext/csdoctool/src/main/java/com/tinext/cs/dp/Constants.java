package com.tinext.cs.dp;

public enum Constants {
    ECM_DOCUMENT_PIPELINE_CONF,
    DT_DOCTOLES,
    DT_SETUP,
    HTTP_USERNAME,
    HTTP_PASSWORD,
    OTAUTHENTICATION,
    AUTHENTICATIONTOKEN,
    PREFIX,
    ECM_API_NAMESPACE,
    CONNECTION_TYPE,
    LLSERVER,
    LLSERVERPORT,
    AUTHWSDL,
    DOCWSDL,
    TYPEOFSERVICE,
    ECM_DOC_NAMESPACE,
    LLNODEINFO,
    NOPARAMS,
    NOTFOUND,
    DONE,
    TINEXT_FIELDS,
    TINEXT_LRID;


    @Override
    public String toString() {
        switch (this) {
            case ECM_DOCUMENT_PIPELINE_CONF:
                return "ECM_DOCUMENT_PIPELINE_CONF";
            case DT_DOCTOLES:
                return "DT_DOCTOLES";
            case DT_SETUP:
                return DT_DOCTOLES.toString() + ".Setup";
            case HTTP_USERNAME:
                return "HTTP_USERNAME";
            case HTTP_PASSWORD:
                return "HTTP_PASSWORD";
            case OTAUTHENTICATION:
                return "OTAuthentication";
            case AUTHENTICATIONTOKEN:
                return "AuthenticationToken";
            case PREFIX:
                return "";
            case ECM_API_NAMESPACE:
                return "urn:api.ecm.opentext.com";
            case CONNECTION_TYPE:
                return "CONNECTION_TYPE";
            case LLSERVER:
                return "LLSERVER";
            case LLSERVERPORT:
                return "LLSERVERPORT";
            case AUTHWSDL:
                return "/cws/authentication.svc?wsdl";
            case DOCWSDL:
                return "/cws/documentmanagement.svc?wsdl";
            case TYPEOFSERVICE:
                return "DocumentManagement";
            case ECM_DOC_NAMESPACE:
                return "urn:DocMan.service.livelink.opentext.com";
            case LLNODEINFO:
                return "LLNODEINFO";
            case NOPARAMS:
                return "parameters are empty or not defined";
            case NOTFOUND:
                return "some of %s statements are missing in COMMANDS file";
            case DONE:
                return "document has been processed";
            case TINEXT_FIELDS:
                return "TINEXT_FIELDS";
            case TINEXT_LRID:
                return "TINEXT_LRID";
            default:
                throw new IllegalArgumentException();
        }
    }
}
