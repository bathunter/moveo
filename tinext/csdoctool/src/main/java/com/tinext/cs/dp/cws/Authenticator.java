package com.tinext.cs.dp.cws;

import com.opentext.livelink.service.core.Authentication;
import com.opentext.livelink.service.core.Authentication_Service;
import com.sun.xml.internal.ws.developer.JAXWSProperties;
import com.tinext.cs.dp.cws.exceptions.AuthenticationException;
import com.tinext.cs.dp.cws.handlers.AuthenticationHandler;
import com.tinext.cs.dp.setup.SetupReader;
import ixos.base.setup.SetupException;

import javax.net.ssl.*;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.soap.SOAPFaultException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import static com.tinext.cs.dp.Constants.*;

public class Authenticator {

    private static final String JAXWS_HOSTNAME_VERIFIER = JAXWSProperties.HOSTNAME_VERIFIER;
    private static final String JAXWS_SSL_SOCKET_FACTORY = JAXWSProperties.SSL_SOCKET_FACTORY;

    private String user;
    private String pass;
    private String authToken;
    private Date expirationDate;

    public Authenticator(String user, String pass) {
        this.user = user;
        this.pass = pass;
    }

    public void init() throws AuthenticationException {
        try {
            String SERVICEWSDL =
                    SetupReader.getVar(CONNECTION_TYPE.toString(), "http") + "://" + SetupReader.getVar(LLSERVER.toString(), "localhost") + ":" + SetupReader.getVar(LLSERVERPORT.toString(), "80") + AUTHWSDL.toString();
            Authentication authClient = createAuthclient(new URL(SERVICEWSDL));
            authToken = null;
            try {
                authToken = authClient.authenticateUser(user, pass);
                if (hasToken()) {
                    ((BindingProvider) authClient).getBinding().setHandlerChain(Collections.singletonList(new AuthenticationHandler(authToken)));
                    expirationDate = toDate(authClient.getSessionExpirationDate());
                }
            } catch (SOAPFaultException e) {
                String msg = String.format("Authenticating failed for user %s, %s", user, e.getMessage());
                throw new AuthenticationException(msg, e);
            }
        } catch (MalformedURLException | SetupException e) {
            String msg = String.format("Incorrect url, check cws property, %s", e.getMessage());
            throw new AuthenticationException(msg, e);
        }
    }

    private Authentication createAuthclient(URL location) throws AuthenticationException {
        Authentication authClient;
        try {
            Authentication_Service authService = new Authentication_Service(location);
            authClient = authService.getBasicHttpBindingAuthentication();
            ((BindingProvider) authClient).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
                    location.toExternalForm());
            //ignore SSL if used
            if (location.getProtocol().equalsIgnoreCase("https")) {
                ((BindingProvider) authClient).getRequestContext().put(JAXWS_SSL_SOCKET_FACTORY,
                        getTrustingSSLSocketFactory());
                ((BindingProvider) authClient).getRequestContext().put(JAXWS_HOSTNAME_VERIFIER,
                        (HostnameVerifier) (hostName, session) -> true);
            }
            ((BindingProvider) authClient).getBinding().setHandlerChain(new ArrayList<>());
        } catch (IllegalArgumentException | NullPointerException | UnsupportedOperationException | ClassCastException | WebServiceException e) {
            String msg = String.format("Authentication error, %s", e.getMessage());
            throw new AuthenticationException(msg, e);
        }
        return authClient;
    }

    public String getAuthToken() {
        return (authToken == null ? "" : authToken);
    }

    public Boolean hasToken() {
        return authToken != null;
    }

    private Date toDate(XMLGregorianCalendar calendar) {
        return (calendar == null) ? null : calendar.toGregorianCalendar().getTime();
    }

    public Date getExpDate() {
        return expirationDate;
    }

    private SSLSocketFactory getTrustingSSLSocketFactory() throws AuthenticationException {
        SSLContext sslContext;
        try {
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, new TrustManager[]{new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] x509Certificates, String s) {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] x509Certificates, String s) {
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            }}, new SecureRandom());
            return sslContext.getSocketFactory();
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            throw new AuthenticationException("Failed to initialize SSL context.");
        }
    }
}
