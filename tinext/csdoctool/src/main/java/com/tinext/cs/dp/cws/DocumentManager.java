package com.tinext.cs.dp.cws;

import com.opentext.livelink.service.core.DataValue;
import com.opentext.livelink.service.core.StringValue;
import com.opentext.livelink.service.docman.DocumentManagement;
import com.opentext.livelink.service.docman.DocumentManagement_Service;
import com.sun.xml.internal.ws.developer.JAXWSProperties;
import com.tinext.cs.dp.cws.exceptions.DocumentManagementException;
import com.tinext.cs.dp.cws.handlers.AuthenticationHandler;
import com.tinext.cs.dp.setup.SetupReader;
import ixos.base.setup.SetupException;

import javax.net.ssl.*;
import javax.xml.namespace.QName;
import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.soap.SOAPFaultException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import static com.tinext.cs.dp.Constants.*;

public class DocumentManager {

    private static final String JAXWS_HOSTNAME_VERIFIER = JAXWSProperties.HOSTNAME_VERIFIER;
    private static final String JAXWS_SSL_SOCKET_FACTORY = JAXWSProperties.SSL_SOCKET_FACTORY;

    private DocumentManagement client;

    public void init(String authToken) throws DocumentManagementException {
        try {
            String SERVICEWSDL =
                    SetupReader.getVar(CONNECTION_TYPE.toString(), "http") + "://" + SetupReader.getVar(LLSERVER.toString(), "localhost") + ":" + SetupReader.getVar(LLSERVERPORT.toString(), "80") + DOCWSDL.toString();
            URL url = new URL(SERVICEWSDL);
            DocumentManagement_Service docManService = new DocumentManagement_Service(url,
                    new QName(ECM_DOC_NAMESPACE.toString(), TYPEOFSERVICE.toString()));
            client = docManService.getBasicHttpBindingDocumentManagement();
            ((BindingProvider) client).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
                    url.toExternalForm());
            //ignore SSL if used
            if (url.getProtocol().equalsIgnoreCase("https")) {
                ((BindingProvider) client).getRequestContext().put(JAXWS_SSL_SOCKET_FACTORY,
                        getTrustingSSLSocketFactory());
                ((BindingProvider) client).getRequestContext().put(JAXWS_HOSTNAME_VERIFIER,
                        (HostnameVerifier) (hostName, session) -> true);
            }
            try {
                Binding binding = ((BindingProvider) client).getBinding();
                List<Handler> handlers = binding.getHandlerChain();
                handlers.add(new AuthenticationHandler(authToken));
                binding.setHandlerChain(handlers);
            } catch (Exception e) {
                String msg = String.format("Failed to set document SOAP header, %s", e.getMessage());
                throw new DocumentManagementException(msg, e);
            }
        } catch (MalformedURLException | SetupException e) {
            String msg = String.format("Incorrect url, check cws property, %s", e.getMessage());
            throw new DocumentManagementException(msg, e);
        }
    }

    private Boolean isAlive() {
        return client != null;
    }


    private void executeReport(long reportID, List<DataValue> inputs) throws DocumentManagementException {
        if (isAlive()) {
            try {
                client.runReport(reportID, inputs);
            } catch (SOAPFaultException e) {
                throw new DocumentManagementException(String.format("%s : %s\n", e.getFault().getFaultCode(),
                        e.getMessage()), e);
            }
        }
    }

    public <T> void runReport(long reportID, List<T> inputs) throws DocumentManagementException {
        //Make use of StringValue because it seems that for CS it doesn't matter what type we are using for inputs :)
        List<DataValue> dataValues = new ArrayList<>();
        for (int i = 0; i < inputs.size(); i++) {
            StringValue strVal = new StringValue();
            strVal.getValues().add(inputs.get(i).toString());
            strVal.setKey("InputLabel" + (i + 1));
            dataValues.add(strVal);
        }
        executeReport(reportID, dataValues);
    }


    private SSLSocketFactory getTrustingSSLSocketFactory() throws DocumentManagementException {
        SSLContext sslContext;
        try {
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, new TrustManager[]{new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] x509Certificates, String s) {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] x509Certificates, String s) {
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            }}, new SecureRandom());
            return sslContext.getSocketFactory();
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            throw new DocumentManagementException("Failed to initialize SSL context.");
        }
    }
}

























