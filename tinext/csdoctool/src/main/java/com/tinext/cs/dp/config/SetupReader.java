package com.tinext.cs.dp.config;

import ixos.base.log4j.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class SetupReader {
    private final static String CONF_DIR = "ECM_DOCUMENT_PIPELINE_CONF";
    private final static String SETUP_FILE = "DT_DOCTOLES.Setup";
    private final static SetupReader INSTANCE = new SetupReader();
    private final static Log.Context LC = new Log.Context("com.tinext.cs.dp.config.SetupReader");
    private final java.util.Properties prop = new java.util.Properties();


    private SetupReader() {
        String conf = System.getenv(CONF_DIR);
        String filename = (conf != null) ?
                conf + File.separator + "config" + File.separator + "setup" + File.separator + SETUP_FILE : SETUP_FILE;
        try (InputStream inputStream = new FileInputStream(filename)) {
            prop.load(inputStream);
        } catch (IOException e) {
            LC.error("", String.format("Error reading %s file ", filename), e.getCause());
            System.exit(-1);
        }
    }


    public static String getProperty(String key) {
        String value = INSTANCE.prop.getProperty(key);
        return (null == value) ? "" : value.trim();
    }
}
