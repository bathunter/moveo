package com.tinext.cs.dp;

import com.tinext.cs.dp.clients.Client;
import com.tinext.cs.dp.clients.Csclient;
import com.tinext.cs.dp.setup.SetupPropsReader;
import ixos.base.PwdCrypt;
import ixos.base.setup.SetupException;
import ixos.base.setup.SetupFile;
import ixos.dt2.Commands;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class CsdoctoolTest {

    private final static List<String> KEYS = Arrays.asList("testkey","testkey2","testkey4","testkey6");

    @Test
    void main() {
    }

    @Test
    void testCrypt(){
        System.out.println(PwdCrypt.pwencrypt("admin"));
        System.out.println(PwdCrypt.pwdecrypt("G1A1A66A8C80E01EF"));
    }

    @Test
    void readSetup(){
        String http_user = SetupPropsReader.getProperty("HTTP_USERNAME");
        System.out.println(PwdCrypt.pwdecrypt(http_user));
        String SERVICEWSDL =
                SetupPropsReader.getProperty("CONNECTION_TYPE") + "://" + SetupPropsReader.getProperty("LLSERVER") + ":" + SetupPropsReader.getProperty("LLSERVERPORT1") + "/cws/Authentication.svc?wsdl";
        List<String> KEYS = Arrays.asList("FIELDSMAP","LLPARAMID","LLPARAMDATA","LLNODEINFO");
        System.out.println(SERVICEWSDL);
        System.out.println(KEYS.toString());

    }

    @Test
    void checkEnties() throws IOException {
        Commands commands = new Commands("D:\\tinext\\csdoctool","test");
        commands.add("testkey", "testvalue");
        commands.add("testkey1", "testvalue");
        commands.add("testkey2", "testvalue");
        commands.add("testkey3", "testvalue");
        commands.add("testkey4", "testvalue");
        commands.add("testkey5", "testvalue");
        commands.add("testkey6", "testvalue");
        commands.write();
        HashMap<String,String> hmentries = new HashMap<>();
        commands.getEntries()
                .stream().filter(e ->KEYS.contains(e.getKey()))
                .forEach(e->{
                    hmentries.put(e.getKey(), e.getValue());
                });
        assertTrue(hmentries.keySet().containsAll(KEYS));
          System.out.println(hmentries.toString());
    }

    @Test
    void testClient(){
        Client client = new Csclient();
        assertTrue(client.isTimedOut());
    }

    @Test
    void testDecodeBase64(){
        byte[] ba = Base64.getDecoder().decode("MDBlM2Q4NzctOTE2OS00M2RiLTk5MjEtMzQyMjhjMWEzMGM5");
        assertEquals("00e3d877-9169-43db-9921-34228c1a30c9",new String(ba));
    }

    @Test
    void testRegexO(){
        String str1 = "-2000 367643";
        String str2 = "367643";
        assertEquals(str1.replaceAll("-[0-9]+\\s", ""),"367643");
        assertEquals(str2.replaceAll("-[0-9]+\\s", ""),"367643");
        System.out.println(this.getClass().getCanonicalName());
    }

    @Test
    void testnativeSetup() throws SetupException {
        SetupFile setupFile = new SetupFile("","D:\\tinext\\csdoctool\\DT_DOCTOLES.Setup");
        Map<String,String> vars = setupFile.getVariableMap();
        setupFile.readConfig();
        System.out.println("");
    }

    @Test
    void testParse(){
        String str = "ATTRIBUTES 28062.1|Annotation|string|TklOSkEgVXBkYXRlZCBVcGRhdGVk|";
        List<String> params = new ArrayList<>(Arrays.asList(str.split(""+"\\|" + "*\\|")));
        System.out.println(params);
    }


}