package com.tinext.cs.dp.model;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.Base64;

import static org.junit.jupiter.api.Assertions.*;

class IxattrTest {

    @Test
    void getAttributeValue() throws IOException {
        Ixattr ixattr = new Ixattr(new File("D:\\tinext\\csdoctool").getAbsolutePath());
        ixattr.read();
        assertEquals("367974",ixattr.getValue("reportid"));
//        assertEquals("integer",ixattr.getType("reportid"));
    }

    @Test
    void testStrings(){
        String e = "-2000 drfggfh";
//        System.out.println(e.substring("cmd_".length()));
        System.out.println(fromBase64(e));
    }

    private String fromBase64(String encoded) {
        if(encoded.matches("[0-9]+")){
            return encoded;
        }
        try {
            return new String(Base64.getDecoder().decode(encoded));
        } catch (IllegalArgumentException e) {
            return encoded;
        }
    }
}